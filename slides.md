---
marp: true
title: Marp CLI exemple
description: Envoyer vos slides sur le net
theme: uncover
paginate: true
_paginate: false
#customize (https://github.com/marp-team/marp-core/tree/main/themes)
---

![Marp bg 60%](https://raw.githubusercontent.com/marp-team/marp/master/marp.png)

--- 

![bg](./assets/gradient.jpg)

# <!--fit--> Marp CLI exemple

Envoyer vos slides sur le net 😎

https://github.com/yhatt/marp-cli-example

<style scoped>a { color: #eee; }</style>

<!-- This is presenter note. You can write down notes through HTML comment. -->

---

![bg](#123)
![](#fff)

##### <!--fit--> [Marp CLI](https://github.com/marp-team/marp-cli) + [GitLab Pages](https://github.com/pages)
##### <!--fit--> 👉 Le moyen simple pour<br />déposer vos slides sur le net

---

![bg right 60%](https://icongr.am/octicons/file-code.svg)

## **[GitLab Pages](https://gitlab.com/pages)**

#### Prêt pour créer et héberger vos decks!

[![Use this as template h:1.5em](https://img.shields.io/badge/-Use%20this%20as%20template-brightgreen?style=for-the-badge&logo=github)](https://github.com/yhatt/marp-cli-example/generate)

---

![bg](#345)
![](#fff)

![bg right 90%](https://storage.googleapis.com/gweb-uniblog-publish-prod/original_images/tenor_1.gif)

## **[GIF Animés !](https://giphy.com)**

#### Pour animer vos slides avec style 😎
---

## **[Support du code HTML/CSS](https://github.com/marp-team/marp-core/tree/main/themes)**

### La personnalisation à l'infinie !

```html
<style>
    section {
    background-color: #fefefe;
    color: #333;
    }

    img[alt~="center"] {
    display: block;
    margin: 0 auto;
    }
</style>
```



---

### <!--fit--> :ok_hand:

---

![bg 40% opacity blur](https://avatars1.githubusercontent.com/u/3993388?v=4)

### Crédits : Yuki Hattori ([@yhatt](https://github.com/yhatt))

https://github.com/yhatt/marp-cli-example