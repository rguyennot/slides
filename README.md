# MARP - Le deck de slides "As Code" ✍️

[Marp](https://github.com/marp-team/marp) est une collection d’outils open source (Marpit, Marp Core, Marp CLI, Marp for VSCode) qui permet de convertir des fichiers Markdown vers des formats HTML, PDF, PPTX et image(s).  
C'est donc l'outil parfait pour créer son deck de slides en `markdown` dans **VSCode** et oublier définitivement 💩 *PowerPoint* 💩 ! 

# Usage (Docker) 🐳

L'équipe derrière le projet **Marp** a prévue une image Docker si vous ne souhaitez pas installer `node` sur votre laptop.

Le lien vers [Docker Hub](https://hub.docker.com/r/marpteam/marp-cli/)

Les principales commandes à utiliser en local pour construire votre deck : 

```bash
# Convert slide deck into HTML
docker run --rm -v $PWD:/home/marp/app/ -e LANG=$LANG marpteam/marp-cli slide-deck.md

# Convert slide deck into PDF (using Chromium in Docker)
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG marpteam/marp-cli slide-deck.md --pdf

# Convert slide deck into PPTX (using Chromium in Docker)
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG marpteam/marp-cli slide-deck.md --pptx

# Watch mode
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG -p 37717:37717 marpteam/marp-cli -w slide-deck.md

# Server mode (Serve current directory in http://localhost:8080/)
docker run --rm --init -v $PWD:/home/marp/app -e LANG=$LANG -p 8080:8080 -p 37717:37717 marpteam/marp-cli -s .

Refer to GitHub repository for details of usage.
```

# Build automatique ⚙️

Comme l'outil dispose d'une CLI, on peut donc automatiser la génération d'un deck avec une pipeline `.gitlab-ci.yml` : 

```bash
stages:
  - build
  - deploy

[...]

pages:
  stage: deploy
  script:
    # Nothing to do, as we just need to publish the build/ dir
    - mv build public
  artifacts:
    paths:
      - public
  dependencies:
    - generate-deck
```

# Rendu dans GitLab Pages 🖥️

👉 [Demo](https://gitlab.com) dans GitLab Pages

# Marp + VSCode 🔥

Une extension VSCode existe pour **Marp** qui permet d'avoir un aperçu de votre rendu MArkdown directement dans l'IDE.

![marp-extension-vscode](images/marp-vscode-extension.png)
